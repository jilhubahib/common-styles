export default Object.freeze({
  mstsDefaultNavy: '#455A64',
  mstsDefaultRed: '#E57373',
  mstsDefaultGreen: '#66BB6A',
  mstsDefaultYellow: '#FFC107',
  mstsDefaultPurple: '#9FA8DA',
});
