# horizon-vue-common-styles

Shared common style rules between CaaS projects.

## Introduction

This repository contains the implementation of Enterprise style guide.

The technology platform used is Quasar(based on VueJS).

The CSS preprocessor is Stylus.

## Which versions of Quasar?

The package supports Quasar's v15.x and v0.16.x. The respective styles can be found in `/quasar/0.16` (for v0.15.x and v0.16.x) directories.

Any code that can be shared across supported versions lives in `quasar/common`.

## Using in a project

To use in a project, add the following to your `package.json`

    "@caas/common-styles": "git+https://scm.multiservice.com/gitlab/horizon/common-styles.git#3f4077b64064169ef56897ade34721d3de16d034",

Once the first version is released you can start using the version numbers like a standard npm package.

After updating the `package.json`, install it by issuing `yarn install`.

### Quasar v0.15 and v0.16

In your project, create a `app.mat.styl` file and add the following lines:

```stylus
@import '~@caas/common-styles/quasar/0.16/msts/styles/common-overrides/variables.styl'

@import '~@caas/common-styles/quasar/common/variables/app.variables.mat.styl'
@import '~quasar-framework/dist/quasar.mat'
@import '~@caas/common-styles/quasar/0.16/msts/msts.styl'
```

## Overriding Stylus Variables

To define project-specific values for Quasar variables or this project's variables, the variables must be assigned values before the first `import` statement of above example.

```stylus
$primary = #0077C2;
$secondary = #7700C2;
.
.
.
@import '~@caas/common-styles/...'
```

Following css classes have been renamed:

`.no-hover-deco`            -> `.no-hover-decoration`

## Using Stylus variables in single file components (.vue files)

You need to modify the `StlylusLoader` options so that it loads `'~@caas/common-styles/quasar/common/variables/app.variables.mat.styl'`.

- Example configuration in vue-cli generated project (Quasar v0.16) [here](https://scm.multiservice.com/gitlab/BCaaS/bcaas/blob/develop/ui/vue.config.js#L35)

## Contribution

To make a contribution to the repository, create a merge request for the @caas/common-styles repository.
